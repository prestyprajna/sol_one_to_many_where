﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_One_To_Many_Where1.Entity.Interface
{
    public interface ISalesDetailEntity
    {
        int SalesOrderId { get; set; }

        short OrderQty{get;set;}

        decimal UnitPrice{get;set;}
    }
}
