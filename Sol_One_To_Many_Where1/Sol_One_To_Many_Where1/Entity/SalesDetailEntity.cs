﻿using Sol_One_To_Many_Where1.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_One_To_Many_Where1.Entity
{
    public class SalesDetailEntity: ISalesDetailEntity
    {
        public int SalesOrderId { get; set; }

        public short OrderQty { get; set; }

        public decimal UnitPrice { get; set; }
    }
}
