﻿//using Sol_One_To_Many_Where.EF;
using Sol_One_To_Many_Where1.Entity;
using Sol_One_To_Many_Where1.Entity.Interface;
using Sol_One_To_Many_Where1.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_One_To_Many_Where1
{
    public class SalesDal
    {
        #region  declaration

        private AdventureWorks2012Entities db = null;

        #endregion

        #region  constructor

        public SalesDal()
        {
            db = new AdventureWorks2012Entities();
        }

        #endregion

        #region  public methods

        public async Task<IEnumerable<SalesHeaderEntity>> GetSalesHeaderData(SalesHeaderEntity salesHeaderEntityObj)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var getQuery =
                    db
                    ?.SalesOrderHeaders
                    ?.AsEnumerable()
                    ?.Where(
                        (leSalesOrderHeaderObj)=>leSalesOrderHeaderObj.SalesOrderID==salesHeaderEntityObj.SalesOrderId
                        )
                    ?.Select((leSalesOrderHeaderObj) => new SalesHeaderEntity()
                    {
                        SalesOrderId = leSalesOrderHeaderObj.SalesOrderID,
                        SalesOrderNo = leSalesOrderHeaderObj?.SalesOrderNumber,
                        salesOrderDetailEntityObj = this.GetSalesDetailData(leSalesOrderHeaderObj).Result

                    })?.ToList();


                    return getQuery;

                    #region  //gettin error in dis segment of code when using interface...
                    //var getQuery =
                    //db
                    //?.SalesOrderHeaders
                    //?.AsEnumerable()
                    //?.Select((leSalesOrderHeaderObj) => new SalesHeaderEntity()
                    //{
                    //    SalesOrderId = leSalesOrderHeaderObj.SalesOrderID,
                    //    SalesOrderNo = leSalesOrderHeaderObj?.SalesOrderNumber,
                    //    salesOrderDetailEntityObj = leSalesOrderHeaderObj?.SalesOrderDetails
                    //                                ?.AsEnumerable()
                    //                                ?.Select((leSalesOrderDetailObj) => new SalesDetailEntity()
                    //                                {
                    //                                    SalesOrderId = leSalesOrderDetailObj.SalesOrderID,
                    //                                    OrderQty = leSalesOrderDetailObj.OrderQty,
                    //                                    UnitPrice = leSalesOrderDetailObj.UnitPrice
                    //                                })
                    //                                ?.ToList()

                    //})
                    //    ?.ToList();

                    //return getQuery;

                    #endregion

                });
            }
            catch (Exception)
            {

                throw;
            }
           

        }

        #endregion

        #region  private methods

        private async Task<List<SalesDetailEntity>> GetSalesDetailData(SalesOrderHeader salesOrderHeaderObj)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var getQuery1 =
                    salesOrderHeaderObj
                    ?.SalesOrderDetails
                    ?.AsEnumerable()
                    ?.Select((leSalesOrderDetailObj) => new SalesDetailEntity()
                    {
                        SalesOrderId = leSalesOrderDetailObj.SalesOrderID,
                        OrderQty = leSalesOrderDetailObj.OrderQty,
                        UnitPrice = leSalesOrderDetailObj.UnitPrice
                    })
                        ?.ToList();

                    return getQuery1;
                });
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        #endregion
    }
}
