﻿using Sol_One_To_Many_Where1.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_One_To_Many_Where1
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async() =>
            {
                try
                {
                    IEnumerable<SalesHeaderEntity> salesHeaderEntityObj = await new SalesDal().GetSalesHeaderData(new SalesHeaderEntity()
                    {
                        SalesOrderId = 43660
                    }); 
                   
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
                
            }).Wait();
        }
    }
}
